package com.pixonic.exam.task.second;

import com.pixonic.exam.task.second.handler.SimpleHandler;
import com.pixonic.exam.task.second.task.SimpleTask;

import java.time.LocalDateTime;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 12/22/2016
 * Time: 11:15 PM
 * To change this template use File | Settings | File and Code Templates.
 */
public class Application {
    public static void main(String[] args) {
        Handler handler = new SimpleHandler();
        handler.execute(new SimpleTask(() -> 0, LocalDateTime.now().minusSeconds(10)));
        handler.execute(new SimpleTask(() -> 0, LocalDateTime.now().minusSeconds(10)));
        handler.execute(new SimpleTask(() -> 1, LocalDateTime.now().plusSeconds(1)));
        handler.execute(new SimpleTask(() -> 5, LocalDateTime.now().plusSeconds(5)));
        handler.execute(new SimpleTask(() -> 2, LocalDateTime.now().plusSeconds(2)));
    }
}
