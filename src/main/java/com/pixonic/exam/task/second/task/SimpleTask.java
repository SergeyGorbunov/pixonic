package com.pixonic.exam.task.second.task;

import com.pixonic.exam.task.second.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.util.concurrent.Callable;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 12/23/2016
 * Time: 11:37 AM
 * To change this template use File | Settings | File and Code Templates.
 */
public class SimpleTask implements Task {
    private static final Logger LOG = LoggerFactory.getLogger(SimpleTask.class);

    private Callable callable;
    private LocalDateTime time;

    public SimpleTask(Callable callable, LocalDateTime time) {
        this.callable = callable;
        this.time = time;
    }

    @Override
    public Callable getCallable() {
        return callable;
    }

    @Override
    public LocalDateTime getTime() {
        return time;
    }

    @Override
    public String toString() {
        return "SimpleTask{" +
                "time=" + time +
                '}';
    }
}
