package com.pixonic.exam.task.second.handler;

import com.pixonic.exam.task.second.Handler;
import com.pixonic.exam.task.second.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 12/23/2016
 * Time: 11:54 AM
 * To change this template use File | Settings | File and Code Templates.
 */
public class SimpleHandler implements Handler {
    private static final Logger LOG = LoggerFactory.getLogger(SimpleHandler.class);

    private ScheduledExecutorService executor;

    public SimpleHandler() {
        executor = Executors.newSingleThreadScheduledExecutor();
    }

    @Override
    public void execute(Task task) {
        executor.schedule(task.getCallable(), getWaiting(task), TimeUnit.SECONDS);
    }

    private long getWaiting(Task task) {
        return ChronoUnit
                .SECONDS
                .between(LocalDateTime.now(), task.getTime());
    }
}
