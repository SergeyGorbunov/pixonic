package com.pixonic.exam.task.second;

import java.time.LocalDateTime;
import java.util.concurrent.Callable;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 12/23/2016
 * Time: 3:38 AM
 * To change this template use File | Settings | File and Code Templates.
 */
public interface Task<V> {

    Callable<V> getCallable();

    LocalDateTime getTime();
}
