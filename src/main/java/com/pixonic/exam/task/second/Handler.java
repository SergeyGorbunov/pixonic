package com.pixonic.exam.task.second;

/**
 * Created by IntelliJ IDEA.
 * User: Sergey Gorbunov(gorseraver@gmail.com)
 * Date: 12/23/2016
 * Time: 3:37 AM
 * To change this template use File | Settings | File and Code Templates.
 */
public interface Handler {
    void execute(Task task);
}
